from sqlalchemy import Column, Integer, String, ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship
from .base import Base


class MachineItemM2M(Base):
    __tablename__ = 'machine_item_m2m'
    id = Column(Integer, primary_key=True, index=True)
    machine_id = Column(Integer, ForeignKey('machines.id'))
    item_id = Column(Integer, ForeignKey('items.id'))
    stock_available = Column(Integer)

# coin and bank using for calculate change money as well but I didn't have time to do that
class MachineModel(Base):
    __tablename__ = "machines"

    id = Column(Integer, primary_key=True, index=True)
    items = relationship('ItemModel', secondary=MachineItemM2M.__table__, backref='MachineModel')
    name = Column(String, index=True)
    coin_1 = Column(Integer, default=0)
    coin_5 = Column(Integer, default=0)
    coin_10 = Column(Integer, default=0)
    bank_20 = Column(Integer, default=0)
    bank_50 = Column(Integer, default=0)
    bank_100 = Column(Integer, default=0)
    bank_500 = Column(Integer, default=0)
    bank_1000 = Column(Integer, default=0)
