from pydantic import BaseModel
from .item import Item


class MoneyBase(BaseModel):
    coin_1: int | None = 0
    coin_5: int | None = 0
    coin_10: int | None = 0
    bank_20: int | None = 0
    bank_50: int | None = 0
    bank_100: int | None = 0
    bank_500: int | None = 0
    bank_1000: int | None = 0


class MachineBase(MoneyBase):
    name: str


class MachineCreate(MachineBase):
    pass


class Machine(MachineBase):
    id: int
    items: list[Item]

    class Config:
        orm_mode = True


class MachineAddItem(BaseModel):
    item_id: int
    stock_available: int


class MachineUpdateItem(MachineAddItem):
    machine_id: int


class BuyItem(MoneyBase):
    machine_id: int
    item_id: int
    quantity: int
