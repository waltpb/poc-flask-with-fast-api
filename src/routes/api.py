from fastapi import APIRouter
from src.endpoints import item, machine

router = APIRouter()
router.include_router(item.router)
router.include_router(machine.router)
