from fastapi import APIRouter, Depends, Query, HTTPException
from src.models.item import ItemModel
from src.schemas.item import Item, ItemCreate, ItemBase
from sqlalchemy.orm import Session
from .base import get_db

router = APIRouter(
    prefix="/items",
    tags=["Item"],
    responses={404: {"description": "Not found"}},
)

@router.get("/")
def get_items(db: Session = Depends(get_db)):
    items = db.query(ItemModel).all()
    if len(items) <= 0:
        raise HTTPException(status_code=200, detail="Items was empty")
    return items

@router.get("/{item_id}")
def get_item(item_id: int, db: Session = Depends(get_db)):
    item = db.query(ItemModel).filter(ItemModel.id == item_id).first()
    if item is None:
        raise HTTPException(status_code=400, detail="Item not found")
    return item

@router.post("/", response_model=Item)
def create_item(item: ItemCreate, db: Session = Depends(get_db)):
    db_item = ItemModel(**item.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

@router.put("/{item_id}")
def update_item(item_id: int, item: ItemBase, db: Session = Depends(get_db)):
    item = db.query(ItemModel).filter(ItemModel.id == item_id).update(item.dict())
    if item:
        db.commit()
        return {"message": "Item update successfully"}
    else:
        return HTTPException(status_code=400, detail="Can't update item")

@router.delete("/{item_id}")
def delete_item(item_id: int, db: Session = Depends(get_db)):
    item = db.query(ItemModel).filter(ItemModel.id == item_id).delete()
    if item:
        db.commit()
        return {"message": "Item deleted"}
    else:
        return HTTPException(status_code=400, detail="Can't delete item")
