from fastapi import APIRouter, Depends, Query, HTTPException
from src.models.machine import MachineModel, MachineItemM2M
from src.models.item import ItemModel
from src.schemas.machine import Machine, MachineCreate, MachineBase, MachineAddItem, MachineUpdateItem, BuyItem, MoneyBase
from sqlalchemy.orm import Session
from .base import get_db
from .item import get_item

router = APIRouter(
    prefix="/machines",
    tags=["Machine"],
    responses={404: {"description": "Not found"}},
)

listMoney: dict = {
    "coin_1": 1,
    "coin_5": 5,
    "coin_10": 10,
    "bank_20": 20,
    "bank_50": 50,
    "bank_100": 100,
    "bank_500": 500,
    "bank_1000": 1000
}

def calculate_money(data: MoneyBase) -> int:
    return sum(data[k] * v for k,v in listMoney.items())

def calculate_change_money(amount: int, machine: Machine, data: MoneyBase) -> tuple[dict, dict]:
    print(amount)
    can_change: bool = False
    change_obj: dict = {}
    update_obj: dict = {}
    # If you won't need to change money with bank just remove bank in this list
    for k,v in reversed(listMoney.items()):
        require_qty = int(amount / v)
        if (require_qty >= 1):
            qty = getattr(machine, k)
            if (qty >= require_qty):
                change_obj[k] = require_qty
                amount -= require_qty * v
                update_obj[k] = qty - require_qty
            elif (qty > 0):
                adjust_qty = require_qty - qty
                change_obj[k] = qty if adjust_qty > 0 else adjust_qty
                update_obj[k] = 0 if adjust_qty > 0 else adjust_qty
                amount -= change_obj[k] * v
            else:
                update_obj[k] = qty
            update_obj[k] += data.get(k) or 0
        else:
            update_obj[k] = getattr(machine, k) + (data.get(k) or 0)
        if amount == 0:
            can_change = True
            break
    if (can_change):
        return (change_obj, update_obj)
    else:
        raise HTTPException(status_code=400, detail="Machine doesn't have money enough for change money")

@router.get("/m2m")
def get_m2m(db: Session = Depends(get_db)):
    m2m = db.query(MachineItemM2M).all()
    if len(m2m) <= 0:
        raise HTTPException(status_code=200, detail="Data was empty")
    return m2m

@router.post("/add_item/{machine_id}")
def add_item_machine(machine_id: int, item: MachineAddItem, db: Session = Depends(get_db)):
    db_m2m = MachineItemM2M(**{
        "machine_id": machine_id,
        **item.dict()
    })
    db.add(db_m2m)
    db.commit()
    db.refresh(db_m2m)
    return db_m2m

@router.post("/update_item")
def update_item_machine(req: MachineUpdateItem, db: Session = Depends(get_db)):
    db_m2m = db.query(MachineItemM2M).filter(
        MachineItemM2M.machine_id == req.machine_id,
        MachineItemM2M.item_id == req.item_id
    ).update({"stock_available": req.stock_available})
    if db_m2m:
        db.commit()
        return {"message": "Update successfully"}
    else:
        return HTTPException(status_code=400, detail="Can't update machine")

@router.get("/", response_model=list[Machine])
def get_machines(db: Session = Depends(get_db)):
    machines = db.query(MachineModel).all()
    if len(machines) <= 0:
        raise HTTPException(status_code=200, detail="Machines was empty")
    return machines

@router.get("/{machine_id}", response_model=Machine)
def get_machine(machine_id: int, db: Session = Depends(get_db)):
    machine = db.query(MachineModel).filter(MachineModel.id == machine_id).first()
    if machine is None:
        raise HTTPException(status_code=400, detail="Machine not found")
    return machine

@router.post("/", response_model=Machine)
def create_machine(machine: MachineCreate, db: Session = Depends(get_db)):
    db_machine = MachineModel(**machine.dict())
    db.add(db_machine)
    db.commit()
    db.refresh(db_machine)
    return db_machine

@router.put("/{machine_id}")
def update_machine(machine_id: int, machine: MachineBase, db: Session = Depends(get_db)):
    machine = db.query(MachineModel).filter(MachineModel.id == machine_id).update(machine.dict())
    if machine:
        db.commit()
        return {"message": "Machine update successfully"}
    else:
        raise HTTPException(status_code=400, detail="Can't update machine")

@router.delete("/{machine_id}")
def delete_machine(machine_id: int, db: Session = Depends(get_db)):
    machine = db.query(MachineModel).filter(MachineModel.id == machine_id).delete()
    if machine:
        db.commit()
        return {"message": "Machine deleted"}
    else:
        return HTTPException(status_code=400, detail="Can't delete machine")

@router.post("/buy_item")
def buy_item(data: BuyItem, db: Session = Depends(get_db)):
    m2m = db.query(MachineItemM2M).filter(
        MachineItemM2M.machine_id == data.machine_id,
        MachineItemM2M.item_id == data.item_id
    ).first()
    if m2m is None:
        raise HTTPException(status_code=400, detail="Item is not available")
    elif m2m.stock_available < data.quantity:
        raise HTTPException(status_code=400, detail="Item is insufficient")
    item = get_item(data.item_id, db)
    money = calculate_money(data.dict())
    remaining_money = money - (item.price * data.quantity)
    if remaining_money < 0:
        raise HTTPException(status_code=400, detail="Your money doesn't enough")
    machine = get_machine(data.machine_id, db)
    change_obj, update_obj = calculate_change_money(remaining_money, machine, data.dict())
    machine_update = db.query(MachineModel).filter(MachineModel.id == machine.id).update(update_obj)
    m2m_update = db.query(MachineItemM2M).filter(
        MachineItemM2M.machine_id == data.machine_id,
        MachineItemM2M.item_id == data.item_id
    ).update({"stock_available": (m2m.stock_available - data.quantity)})
    if (m2m_update and machine_update):
        db.commit()
        return {"change_money": {**change_obj}, "message": "Payment successfully"}
    else:
        raise HTTPException(status_code=400, detail="Can't process payment")
