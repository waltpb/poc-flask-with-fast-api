from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.routes.api import router
from src.models.base import Base, engine
import uvicorn

app = FastAPI()

Base.metadata.create_all(bind=engine)

origins = ["http://localhost:3000"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(router)

if __name__ == '__main__':
    uvicorn.run("main:app", host='127.0.0.1', port=3000, log_level="info", reload=True)
    print("running")
